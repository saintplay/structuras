enum TIPO_POSTRE{VEGETAL, FRUTA, MENESTRA, CARNE, POLLO, PESCADO, CEREAL};

class Ingrediente
{
private:
    int codigo;
    TIPO_POSTRE tipo;
    char* nombre;
    int cantidad;
public:
    static Ingrediente* registrar();

    Ingrediente();
    Ingrediente(int codigo, TIPO_POSTRE tipo, char* nombre, int cantidad);
    ~Ingrediente();

    void setCodigo(int codigo);
    void setTipo(TIPO_POSTRE tipo);
    void setNombre(char* nombre);
    void setCantidad(int cantidad);

    int getCodigo();
    TIPO_POSTRE getTipo();
    char* getNombre();
    int getCantidad();

    int getCalorias();
};
