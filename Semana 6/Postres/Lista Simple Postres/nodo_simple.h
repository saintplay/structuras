#include "fstream"
#include "nodo.h"
#include <iostream>

using namespace std;

template <class C>
class Nodos
{
private:
    Nodo<C>* inicio;

public:

    Nodos() {
        inicio = nullptr;
    }

    ~Nodos() = default;

    void insertarDato(C* dato) {
        Nodo<C>* nuevo_nodo = new Nodo<C>(dato);

        if (inicio == nullptr) {
            inicio = nuevo_nodo;
        }
        cout<<"Archivo creado!"<< endl;
        ofstream archivo;
        archivo.open("nodo_simple.csv", ios::out | ios::app);
        archivo << dato->getCodigo() << "," << dato->getNombre() << "," << dato->getCantidad() << endl;
        archivo.close();
    }
};

