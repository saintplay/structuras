#include "postre.h"
#include <string.h>

Postre::Postre(char* nombre, int codigo) {
    this->nombre = nombre;
    this->codigo = codigo;
}

Postre::~Postre() {

}

void Postre::setNombre(char* nombre) {
    strcpy(this->nombre, nombre);
}

char* Postre::getNombre() {
    return nombre;
}
