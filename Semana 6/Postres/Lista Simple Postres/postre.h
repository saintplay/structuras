#include "ingredientes.h"

class Postre
{
private:
    char* nombre;
    int codigo;
public:
    Postre(char* nombre, int codigo);
    ~Postre();

    void setNombre(char* nombre);
    char* getNombre();
};
