#include <iostream>
#include "nodo_simple.h"
#include "ingrediente.h"

using namespace std;

Nodos<Ingrediente>* ingredientes;

int mostrarMenu() {

    int opcion;

    cout << "========= MENU =========" << endl;
    cout << "1: Agregar Ingrediente" << endl;
    cout << "2: Mostrar Ingredientes" << endl;
    cout << "3: Terminar el programa" << endl;
    cout << "Ingrese su opcion: ";
    cin >> opcion;

    return opcion;
}

int main()
{
    ingredientes = new Nodos<Ingrediente>();
    int opcion;

    do {
        opcion = mostrarMenu();
        Ingrediente* nuevo_ingrediente;

        switch (opcion) {
        case 1:
            nuevo_ingrediente = Ingrediente::registrar();
            ingredientes->insertarDato(nuevo_ingrediente);
            break;
        case 2:
            break;
        case 3:
            break;
        default:
            cout << "[ERROR] No se reconocio la opcion ingresada!" << endl;
        }
    } while (opcion != 3);


    return 0;
}
