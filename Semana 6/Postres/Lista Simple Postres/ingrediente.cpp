#include "ingrediente.h"
#include <string.h>
#include <iostream>

using namespace std;

Ingrediente::Ingrediente() { nombre = new char[50]; }

Ingrediente* Ingrediente::registrar() {
    Ingrediente* ingrediente = new Ingrediente();
    int codigo, cantidad;
    char nombre[50];

    cout << "Ingrese el CODIGO del ingrediente: ";
//    cin >> codigo;
    ingrediente->setCodigo(codigo);
    cout << "Ingrese el NOMBRE del ingrediente: ";
    cin >> nombre;
    ingrediente->setNombre(nombre);
    cout << "Ingrese la CANTIDAD que se necesita del ingrediente: ";
    cin >> cantidad;
    ingrediente->setCantidad(cantidad);

    return ingrediente;
}

Ingrediente::Ingrediente(int codigo, TIPO_POSTRE tipo, char* nombre, int cantidad) {
    this->codigo = codigo;
    this->tipo = tipo;
    strcpy(this->nombre, nombre);
    this->cantidad = cantidad;
}

Ingrediente::~Ingrediente() {
    delete[] nombre;
}

void Ingrediente::setCodigo(int codigo) { this->codigo = codigo; }
void Ingrediente::setTipo(TIPO_POSTRE tipo) { this->tipo = tipo; }
void Ingrediente::setNombre(char* nombre) { strcpy(this->nombre, nombre );
}
void Ingrediente::setCantidad(int cantidad) { this->cantidad = cantidad; }

int Ingrediente::getCodigo() { return codigo; }
TIPO_POSTRE Ingrediente::getTipo() { return tipo; }
char* Ingrediente::getNombre() { return nombre; }
int Ingrediente::getCantidad() { return cantidad; }


int Ingrediente::getCalorias() {
    switch (tipo) {
    case VEGETAL:
        return cantidad * 10;
        break;
    case FRUTA:
        return cantidad * 20;
        break;
    case MENESTRA:
        return cantidad * 15;
        break;
    case CARNE:
        return cantidad * 30;
        break;
    case POLLO:
        return cantidad * 30;
        break;
    case PESCADO:
        return cantidad * 25;
        break;
    case CEREAL:
        return cantidad * 12;
        break;
    }
}
