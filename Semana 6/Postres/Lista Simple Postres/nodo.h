template <class C>
class Nodo
{
private:
    C* dato;
    Nodo<C>* siguiente;
public:

    Nodo(C* dato) {
        this->dato = dato;
        this->siguiente = nullptr;
    }

    Nodo(C* dato, Nodo<C>* siguiente) {
        this->dato = dato;
        this->siguiente = siguiente;
    }

    ~Nodo() = default;

    void setDato(C* dato) { this->dato = dato; }
    void setSiguiente(Nodo<C>* dato) {this->siguiente = siguiente; }

    C* getDato() { return dato; }
    Nodo<C>* getSiguiente() { return siguiente; }
};
