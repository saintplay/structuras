#include "NodoDoble.h"
#include <iostream>

using namespace std;

template <typename T>
class Nodos
{
private:
    NodoDoble<T>* inicio;

public:

    Nodos() {
        this->inicio = NULL;
    }

    ~Nodos() {
    }

    void mostrar() {

        NodoDoble<T>* nodo = inicio;

        cout<< nodo->getDato() << endl;
        nodo = nodo->getSiguiente();

        while (nodo != inicio) {
            cout << nodo->getDato() << endl;
            nodo = nodo->getSiguiente();
        }
    }

    void insertarFinal(T dato) {

        NodoDoble<T>* nodo = inicio;

        while (nodo->getSiguiente() != inicio) {
            nodo = nodo->getSiguiente();
        }

        NodoDoble<T>* nuevo_nodo = new NodoDoble<int>(dato, nodo, inicio);
        nodo->setSiguiente(nuevo_nodo);
    }
};
