template <typename T>
class NodoDoble {
private:
    T dato;
    NodoDoble<T>* anterior;
    NodoDoble<T>* siguiente;

public:
    NodoDoble(T dato, NodoDoble<T>* anterior, NodoDoble<T>* siguiente) {
        this->dato = dato;
        this->anterior = anterior;
        this->siguiente = siguiente;
    }

    ~NodoDoble() {
        if (anterior != NULL) {
            delete anterior;
        }

        if (siguiente != NULL) {
            delete siguiente;
        }
    }

    T getDato() {
        return dato;
    }

    NodoDoble<T>* getAnterior() {
        return anterior;
    }

    NodoDoble<T>* getSiguiente() {
        return siguiente;
    }

     void setAnterior(NodoDoble<T>* nuevo_nodo) {
        delete this->anterior;
        this->anterior = nuevo_nodo;
    }

    void setSiguiente(NodoDoble<T>* nuevo_nodo) {
        delete this->siguiente;
        this->siguiente = nuevo_nodo;
    }
};
