#include <stdio.h>

int main() {

	int numero1, numero2;

	printf("Ingrese el primer numero: ");
	int success = scanf("%d", &numero1);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Ingrese el segundo numero: ");
	success = scanf("%d", &numero2);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	int suma = numero1 + numero2;

	printf("La sumatoria de %d y %d es %d\n", numero1, numero2, suma);

	return 0;
}

