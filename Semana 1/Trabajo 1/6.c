#include <stdio.h>

int main() {

	int numeros[5];
	int maximo;
	int indice;

	for (int i = 0; i < 5; i++) {
		
		printf("Ingrese el numero %d: ", i + 1);
		
		int success = scanf("%d", &numeros[i]);

		if (success == 0) {
			printf("No se ingreso un numero valido\n");
			return 0;
		}

		if (i == 0) {
			maximo = numeros[0];
			indice = 0;
		} else if (numeros[i] > maximo) {
			maximo = numeros[i];
			indice = i;
		}
	}

	printf("El maximo es el numero %d: %d\n", indice + 1, maximo);

	return 0;
}

