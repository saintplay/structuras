#include <stdio.h>

int main() {

	int numero;
	
	printf("Ingrese un numero: ");
		
	int success = scanf("%d", &numero);

	if (success == 0) {
		printf("No se ingreso una numero valido\n");
		return 0;
	}

	printf("Tabla de multiplicar del %d:\n", numero);
	
	for (int i = 0; i <= 10; i++) {
		printf(" - %d x %d = %d\n", i, numero, i * numero);
	}

	return 0;
}

