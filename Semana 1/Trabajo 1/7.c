#include <stdio.h>

int main() {

	float nota;
	
	printf("Ingrese una nota: ");
		
		int success = scanf("%f", &nota);

		if (success == 0 || nota < 0 || nota > 20) {
			printf("No se ingreso una nota valido\n");
			return 0;
		}

		if (nota < 10.5) {
			printf("La nota es calificada como 'Jalada'\n");
		} else if (nota < 15) {
			printf("La nota es calificada como 'Aprobada'\n");
		} else if (nota < 17) {
			printf("La nota es calificada como 'Sobresaliente'\n");
		} else {
			printf("La nota es calificada como 'Matricula de Honor'\n");
		}

	return 0;
}

