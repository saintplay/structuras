#include <stdio.h>

int main() {

	int edad1, edad2;

	printf("Ingrese la edad de la primera persona: ");
	int success = scanf("%d", &edad1);

	if (success == 0) {
		printf("No se ingreso un numero\n");
		return 0;
	}

	printf("Ingrese la edad de la segunda persona: ");
	success = scanf("%d", &edad2);

	if (success == 0) {
		printf("No se ingreso un numero\n");
		return 0;
	}

	if (edad1 > edad2) {
		printf("La primera edad es mayor(%d>%d)\n", edad1,edad2);
	} else if (edad1 < edad2) {
		printf("La segunda edad es mayor (%d>%d)\n", edad2,edad1);
	} else {
		printf("Las dos edades son iguales (%d=%d)\n", edad1,edad2);
	}

	return 0;
}

