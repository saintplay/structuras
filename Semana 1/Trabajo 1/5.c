#include <stdio.h>

int main() {

	int numero1, numero2;

	printf("Ingrese el primer numero: ");
	int success = scanf("%d", &numero1);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Ingrese el segundo numero: ");
	success = scanf("%d", &numero2);

	if (success == 0) {
		printf("No se ingreso un numero\n");
		return 0;
	}

	if (numero1 > numero2) { 
		printf("La primera numero es mayor(%d>%d)\n", numero1, numero2);
	} else if (numero1 < numero2) {
		printf("La segunda numero es mayor (%d>%d)\n", numero2, numero1);
	} else {
		printf("Las dos numeros son iguales (%d=%d)\n", numero1, numero2);
	}
	return 0;
}

