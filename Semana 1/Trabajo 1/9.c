#include <stdio.h>
#include <math.h>

int main() {

	printf("Potencias de 2 entre 20 y 230)\n");

	int exponente = 0;
	int potencia = 1;
	
	while (potencia <= 230) {
		if (potencia >= 20) {
			printf(" -  %d\n", potencia);
		}
		exponente++;
		potencia = pow(2, exponente);
	}

	return 0;
}

