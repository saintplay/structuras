#include <stdio.h>

int main() {

	int numero;
	
	printf("Ingrese un numero: ");
		
	int success = scanf("%d", &numero);

	if (success == 0 || numero < 0) {
		printf("No se ingreso una numero valido\n");
		return 0;
	}

	int factorial = 1;

	for (int i = 1; i <= numero; i++) {
		factorial *= i;
	}

	printf("El factorial de %d(%d!) es: %d\n", numero, numero, factorial);
	return 0;
}

