#include <stdio.h>

int main() {
	
	int numero;

	printf("Ingrese un numero: ");

	int success = scanf("%d", &numero);

	if (success == 0) {
		printf("No se ingreso un numero\n");
		return 0;
	}

	if (numero % 2 == 0) {
		printf("%d es un numero par\n", numero);
	} else {
		printf("%d es un numero impar\n", numero);
	}

	return 0;
}

