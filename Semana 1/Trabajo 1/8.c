#include <stdio.h>

int main() {

	int numero1, numero2;

	printf("Ingrese el primer numero: ");
	int success = scanf("%d", &numero1);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Ingrese el segundo numero: ");
	success = scanf("%d", &numero2);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Multiplos de %d entre %d y %d)\n", numero1, numero1, numero1 * numero2);

	int multiplo = 0;
	
	while (multiplo <= numero1 * numero2) {
		printf(" -  %d\n", multiplo);
		multiplo += numero1;
	}
	
	return 0;
}

