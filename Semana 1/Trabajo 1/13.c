#include <stdio.h>

int main() {

	int numero;
	
	printf("Ingrese un numero: ");
		
	int success = scanf("%d", &numero);

	if (success == 0 || numero < 2) {
		printf("No se ingreso una numero valido\n");
		return 0;
	}

	printf("Numeros positivos pares entre 2 y %d:\n", numero);
	
	for (int i = 2; i <= numero; i+=2) {
		printf(" - %d\n", i);
	}

	return 0;
}

