#include <stdio.h>

int main() {

	int numero1, numero2;

	printf("Ingrese el primer numero: ");
	int success = scanf("%d", &numero1);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Ingrese el segundo numero: ");
	success = scanf("%d", &numero2);

	if (success == 0) {
		printf("No se ingreso un numero\n");
		return 0;
	}

	int mayor = numero2;
	int menor = numero1;

	if (numero1 > numero2) { 
		mayor = numero1;
		menor = numero2;
	}

	int sumatoria = 0;

	printf("Numeros positivos pares entre %d y %d:\n", menor, mayor);
	
	for (int i = menor; i <= mayor; i++) {
		if(i % 2 == 0) {
			printf(" - %d\n", i);
			sumatoria += i;
		}
	}

	printf("La sumatoria de tales numeros pares es: %d\n", sumatoria);	

	return 0;
}

