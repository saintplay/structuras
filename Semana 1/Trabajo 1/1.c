#include <stdio.h>

int main() {

	float number; 
	
	printf("Ingrese un numero: ");
	int success = scanf("%f", &number);

	if (success == 0) {
		printf("No se ingreso un numero\n");
	}	else {
		if (number > 0) {
			printf("El numero: %.2f es positivo\n", number);
		}	else if (number < 0) {
			printf("El numero: %.2f es negativo\n", number);
		}	else {
			printf("El numero: %.2f es exactamente 0\n", number);
		}
	}

	return 0;
}

