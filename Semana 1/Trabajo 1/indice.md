# Primer Trabajo
## Ejercicio 1
>Escriba un programa que lea un numero flotante por teclado y muestre por pantalla el “El numero es negativo.” O  “El numero es positivo.”

## Ejercicio 2
>Escriba un programa que lea la edad de dos personas y diga aqui ́en es mas joven, la primera o la segunda. Ten en cuenta que ambas pueden tener la misma edad. En tal caso, hazlo saber con un mensaje adecuado.

## Ejercicio 3
>Escriba un programa que, dado un numero entero, muestre por pantalla el mensaje “El numero es par.” cuando el numero sea par y el mensaje “El numero es impar.” cuando sea impar.

## Ejercicio 4
>Un capital de C euros a un inter ́es del x por cien anual durante n años se convierte en C ∗ (1 +x/100 )^n euros. Diseñe un programa que solicite la cantidad C y el inter ́es x y calcule el capital final solo si x es una cantidad positiva.

## Ejercicio 5
>Escriba un programa que lea dos numeros de la pantalla y devuelva el mayor de ambos, si ambos son iguales debe escribir “Ambos numeros son iguales”.

## Ejercicio 6
>Escribe un programa que calcule el maximo de 5 numeros enteros.

## Ejercicio 7
>Escribe un programa que, dado un numero real que debe representar la calificacion numerica de un examen, proporcione la calificacion cualitativa correspondiente al numero dado. La calificacion cualitativa ser ́a una de las siguientes: “Jalado” (nota menor que 5), “Aprobado” (nota mayor o igual que 10.5, pero menor que 15), “Notable” (nota mayor o igual que 15, pero menor que 17), “Sobresaliente” (nota mayor o igual que 17, pero menor que 20), “Matricula de Honor” (nota 20).

## Ejercicio 8
>Implementa un programa que muestre todos los multiplos de n entre n y m * n, ambos inclusive, donde n y m son numeros introducidos por el usuario.

## Ejercicio 9
>Implementa un programa que muestre todos los numeros potencia de 2 entre 20 y 230, ambos inclusive.

## Ejercicio 10
>Escriba un programa que calcule la sumatoria entre dos numeros.

## Ejercicio 11
>El factorial de n se denota con n!, escriba un programa que pida el valor de n y muestre por pantalla el resultado de calcular n!.

## Ejercicio 12
>Escriba un programa que muestre la tabla de multiplicar de un numero introducido por teclado por el usuario.

## Ejercicio 13
>Escribe un programa que muestre los numeros pares positivos entre 2 y un numero cualquiera que introduzca el usuario por teclado.

## Ejercicio 14
>Haz un programa que pida el valor de dos enteros n y m y calcule el sumatorio de todos los numeros pares comprendidos entre ellos (incluyendolos en el caso de que sean pares).

## Ejercicio 15
>Haz un programa que calcule el maximo comun divisor (mcd) y minimo comun multiplo (mcm) de dos enteros positivos.