#include <stdio.h>
#include <math.h>

int main() {

	float capital;
	int tasa;
	int anios;

	printf("Ingrese el capital: ");

	int success = scanf("%f", &capital);

	if (success == 0 || capital <= 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Ingrese el radio en porcentaje(1-100): ");

	success = scanf("%d", &tasa);

	if (success == 0 || tasa <= 0) {
		printf("No se ingreso un porcentaje valido\n");
		return 0;
	}

	printf("Ingrese el numero de años: ");

	success = scanf("%d", &anios);

	if (success == 0 || anios <= 0) {
		printf("No se ingreso una cantidad de anios valida\n");
		return 0;
	}

	float interes = capital * pow((1 + tasa/100.0f), anios);

	printf("El interes generado es: %f\n", interes);

	return 0;
}

