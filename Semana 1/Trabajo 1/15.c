#include <stdio.h>

int main() {

	int numero1, numero2;

	printf("Ingrese el primer numero: ");
	int success = scanf("%d", &numero1);

	if (success == 0) {
		printf("No se ingreso un numero valido\n");
		return 0;
	}

	printf("Ingrese el segundo numero: ");
	success = scanf("%d", &numero2);

	if (success == 0) {
		printf("No se ingreso un numero\n");
		return 0;
	}

	if (numero1 == numero2){
		printf("El MCD de %d y %d es 1\n", numero1, numero1);	
		printf("El MCM de %d y %d es %d\n", numero2, numero2, numero2);	
	}

	int mayor = numero2;
	int menor = numero1;

	if (numero1 > numero2) { 
		mayor = numero1;
		menor = numero2;
	}

	int mcm = menor * mayor;
	int stop = 0;

	for (int i = 1; i <= mayor && stop == 0; i++) {
		for (int j = 1; j <= i; j++) {
			if (menor * i == mayor * j) {
					mcm = menor * i;
					stop = 1;
					break;
			}
		}
	}

	int mcd = menor * mayor / mcm;

	printf("El MCD de %d y %d es %d\n", menor, mayor, mcd);	
	printf("El MCM de %d y %d es %d\n", menor, mayor, mcm);	
	
	return 0;
}

