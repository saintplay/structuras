# Generalización de Tipos
## Funciones
### 1.- El mayor de un arreglo
```c++
template <typename T>
T numeroMayor(T* arreglo, int size) {
  T mayor = arreglo[0];
  for (int i = 1; i < size; i++) {
    if (arreglo[i] > mayor) {
      mayor = arreglo[i];
    }
  }
  return mayor;
}
```
### 2.- Ordenar un arreglo
```c++
template <typename T>
void ordenarArreglo(T* arreglo, int size) {
  for (int i = 0; i < size - 1; i++) {
    for (int j = i + 1; j < size; j++) {
      if (arreglo[i] > arreglo[j]) {
        T auxiliar = arreglo[j];
        arreglo[j] = arreglo[i];
        arreglo[i] = auxiliar;
      }
    }
  }
}
```
### 3.- Eliminar elemento de arreglo
```c++
template <typename T>
void eliminarElemento(T* arreglo, int size, int posicion) {
  T* auxiliar = new T[size - 1];
  int pos = 0;
  for (int i = 0; i < size; i++) {
    if (i != posicion) {
        auxiliar[pos] = arreglo[i];
        pos++;
    }
  }
  delete arreglo;
  arreglo = auxiliar;
}
```
### 4.- Buscar elemento de arreglo
```c++
template <typename T>
int buscarElemento(T* arreglo, int size, T elemento) {
  for (int i = 0; i < size; i++) {
    if (elemento == arreglo[i]) {
        return i;
    }
  }
  return -1;
}
```
### 5.- Determinar si elemento existe en arreglo
```c++
template <typename T>
bool existeElemento(T* arreglo, int size, T elemento) {
  for (int i = 0; i < size; i++) {
    if (elemento == arreglo[i]) {
        return true;
    }
  }
  return false;
}
```
### 6.- Veces que se repite elemento en arreglo
```c++
template <typename T>
int vecesDeElemento(T* arreglo, int size, T elemento) {
  int veces = 0;
  for (int i = 0; i < size; i++) {
    if (elemento == arreglo[i]) {
        veces++;
    }
  }
  return veces;
}
```
## Clases
### 1.- Punto
```c++
template <typename T>
class Punto {
private:
  T x;
  T y;
public:
  Punto(T x, T y);
  ~Punto();
};
```
### 2.- Poligono
```c++
template <typename T>
class Poligono{
private:
  Punto<T>* puntos;
  int size;
public:
  Poligono(const Punto<T>[]);
  ~Poligono();  
};
```
### 3.- Curva Bezier
```c++
template <typename T>
class Bezier {
private:
  Punto<T> puntoA;
  Punto<T> puntoB;
public:
  Bezier(Punto<T> puntoA, Punto<T> puntoB);
  ~Bezier();
};
```
### 4.- Canvas
```c++
template <typename T>
class Canvas {
private:
  Poligono<T>* poligonos;
  int sizePoligonos;
  Bezier<T>* beziers;
  int sizeBeziers;
public:
  Canvas(const Poligono<T>[] poligonos, const Bezier<T>[]);
  ~Canvas();
};
```