# Tipos de Datos Abstractos
## Ejercicio 1
```c++
struct Fecha {
  unsigned short segundo;
  unsigned short minuto;
  unsigned short hora;
  unsigned short dia;
  unsigned short mes;
  unsigned short anio;
};
```
## Ejercicio 2
```c++
//Template para int, float, double, etc
template<typename T>
struct Complejo {
  T real;
  T imaginario;
};
```
## Ejercicio 3
```c++
struct GranEntero {
  unsigned numero[100];
  unsigned short digitos;
  bool signo;
};
```
## Ejercicio 4
```c++
struct GranDecimal {
  unsigned entero[100];
  unsigned decimal[100];
  unsigned short digitos_entero;
  unsigned short digitos_decimal;
  bool signo;
};
```
## Ejercicio 5
```c++
struct Persona {
  char nombres[50];
  char apellidos[50];
  unsigned short edad;
  unsigned short dni;
  enum {HOMBRE, MUJER, OTRO} sexo;
  Fecha nacimiento;
};
```
## Ejercicio 6
```c++
struct Empleado : Persona {
  unsigned short codigo;
  unsigned double sueldo_base;
  unsigned double sueldo_total;
};
```
## Ejercicio 7
```c++
struct EmpleadoHoras : Empleado {
  unsigned short horas;
};
```
## Ejercicio 8
```c++
struct EmpleadoDestajo : Empleado {
  unsigned short produccion;
};
```
## Ejercicio 9
```c++
struct Alumno : Persona {
  enum {ESCOLAR, UNIVERSITARIO, TECNICO} tipo;
  char nombre_institucion[50];
};
```
## Ejercicio 10
```c++
struct Universitario : Alumno {
  unsigned short codigo;
  unsigned short ciclo;
  char carrera[50];
  unsigned short creditos;
  Fecha ingreso;
};
```
## Ejercicio 11
```c++
struct Personaje {
  enum {ALIADO, ENEMIGO} tipo;
  unsigned short vida;
  bool vivo;
  bool ia;
  enum {NADA,ESPADA,ARCO,FUSIL,PUÑOS,MAGIA} tipo_ataque;
};
```
## Ejercicio 12
```c++
struct Enemigo : Personaje {
  unsigned short nivel;
  enum {CREEP, HEROE, TORRE} tipo_enemigo;
};
```
## Ejercicio 13
```c++
struct Proyectil {
  unsigned short ataque;
  bool destruible;
  bool esquivable;
  unsigned short vida;
  Personaje objetivo;
  Personaje atacante;
};
```
## Ejercicio 14
```c++
struct Nivel {
  char nombre[50];
  unsigned short tiempo_maximo;
  Aliado* aliados;
  Enemigo* enemigos;
};
```
## Ejercicio 15
```c++
struct Mundo {
  char nombre[50];
  enum {AVENTURA, QUIZ, BATALLA} tipo_mundo;
  Nivel* niveles;
};
```
## Ejercicio 16
```c++
struct Juego {
  char nombre[50];
  char autor[50];
  char sello[50];
  enum {AVENTURA, MOBA, FPS, TERROR, EDUCATIVO} tipo;
  enum {A1, E7, TEEN, PLUS18} categoria;
  enum {PC, PS3, PS4, XBOX, WII, DS} consola;
  unsigned short precio;
};
```
## Ejercicio 17
```c++
struct Catalogo {
  char nombre[50];
  Juego* juegos;
};
```