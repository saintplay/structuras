# Crecimiento de Funciones
## Ejercicio 01
>Suma y Promedio de números.

```c++
int s = 0; // 1
double prom = 0; // 1
for (int i = 0; i < n; i++) { //-> n
s = s + A[i]; // 3
prom = s / (double)n; // 3
}
printf("Suma:%d\nProm:%d", s, prom); // 2
```
- **Tiempo detallado**: 6n +4
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejecicio 02
>Suma y Promedio de números (2.0).

```c++
int s = 0; // 1
double prom = 0; // 1
for (int i = 0; i < n; i++) { // n
  s = s + A[i]; // 3
}
prom = s / (double)n; // 3
printf("Suma:%d\nProm:%d", s, prom); // 2
```
- **Tiempo detallado**: 3n + 7
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 03
>Ordenamiento de N números del 1 al 100.

```c++
for (int i = 0; i < n - 1; i++) { // n - 1
  for (int k = i + 1; k < n; k++) { // n / 2
    if (vec[i] > vec[k]) { // 3
      int aux = vec[i]; // 2
      vec[i] = vec[k]; // 3
      vec[k] = aux; // 2
    }
  }
}
```
- **Tiempo detallado**: 5n² - 5n
- **Tiempo asintótico**: O(n² - n)

## Ejercicio 04
>Ordenamiento de N números del 1 al 100 (2.0).

```c++
int frec[101] = {0}; // 1
for (int i = 0; i < n; i++) { // n
  frec[vec[i]]++; //4
}
int pos = 0; // 1
for (int i = 0; i < 101; i++) { // 101
  for (int k = 0; k < frec[i]; k++) { // m
    vec[pos] = i; // 2
    pos++; // 2
  }
}
```
- **Tiempo detallado**: 404m + 4n + 1
- **Tiempo asintótico**: O(m + n)

## Ejercicio 05
>Algoritmo raro - Infinito.

```c++
int i = 0; // 1
int sum = 0; // 1
while (i < 100) { // 100
  if (i % 2 == 0) { // 2
    for (int k = 0; k < n; k++) { // n
      sum += vec[i]; // 3
    }
  } else {
    for (int k = 0; k < i; k++) { // n
      sum += vec[i]; // 3
    }
  }
}
```
- **Tiempo detallado**: 600n + 202
- **Tiempo asintótico**:  O(n) -> *Lineal*

## Ejercicio 06
>Factorial.

```c++
int fact = 1; // 1
for (int i = 2; i < n; i++) { // n - 2
  fact *= i; // 2
}
printf("Factorial: %d", fact); // 1
```
- **Tiempo detallado**: 2n - 2
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 07
>Buscar cadena de máximo 50 caracteres.

```c++
int pos = -1; // 1
for (int i = 0; i < n; i++) { // n
  if (strcmp(vec[i], cadBuscar) == 0) { //51
    pos = i; // 1
    break;
  }
}
```
- **Tiempo detallado**: 52n + 1
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 08
>Buscar el mayor.

```c++
int pos = 0; // 1
for (int i = 1; i < n; i++) { // n - 1
  if (vec[i] > vec[pos]) { // 3
    pos = i; // 1
  }
}
printf("El mayor es: %d", vec[pos]); // 2
```
- **Tiempo detallado**: 4n - 2
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 09
>Otro algoritmo raro – El while es válido?.

```c++
int max = 0; // 1
for (int m = 0; m < n; m++) { // n
  int cont = 0; // 1
  int k = m + 1; // 2
  while (vec[m] <= vec[k]) { // m
    k = k + 1; // 2
    cont++; // 2
  }
  if (cont > max) { // 1
    max = cont; // 1
  }
}
printf("Maximo %d", max); // 1
```
- **Tiempo detallado**: 4mn + 5n + 1
- **Tiempo asintótico**: O(mn + m)

## Ejercicio 10
>Logarítmica.

```c++
int i = 1; // 1
while (i < n) { // n - 1
  if (vec[i] % 2 == 0) { // 3
    i *= 3; // log(3)
  } else {
    i *= 2; // log(2)
  }
```
- **Tiempo detallado**: 3log(3, n - 1) + 3log(2, n - 1) + 1
- **Tiempo asintótico**: O(log(3,n) + log(2,n))

## Ejercicio 11
>Logarítmica (2.0).

```c++
int i = 1; // 1
while (i < n) { // n - 1
  if (vec[i] % 2 == 0) { // 3
    n = n / 3; // log(1/3)
  } else {
    n = n / 2; // log(1/2)
  }
```
- **Tiempo detallado**: 3log(1/3, n - 1) + 3log(1/2, n - 1) + 1
- **Tiempo asintótico**: O(log(1/3,n) + log(1/2,n))

## Ejercicio 12
>Búsqueda binaria – Arreglo ordenado.

```c++
int inf = 0; // 1
int sup = n - 1; // 2
int pos = -1; // 1
while ((pos == -1) && (sup >= inf)) { // n
  int medio = (inf + sup) / 2; // 3
  if (arreglo[medio] == 80) { // 2
    posicion = medio; // 1
  } else if (arreglo[medio] < 80) { // 2
    limiteInferior = medio + 1; // 2
  } else {
    limiteSuperior = medio - 1; // 2
  }
}
```
- **Tiempo detallado**: 11n + 4
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 13
>Encontrar el número mayor en un arreglo de enteros

```c++
int mayor = arreglo[0]; // 2
for (int i = 1; i < size; i++) { // n - 1
  if (arreglo[i] > mayor) { // 2
    mayor = arreglo[i]; // 2
  }
}
printf("El mayor es: %d", mayor); // 1
```
- **Tiempo detallado**: 4n - 1
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 14
>Ordenar un arreglo de números enteros

```c++
for (int i = 0; i < size - 1; i++) { // n -1
  for (int j = i + 1; j < size; j++) { // n/2
    if (arreglo[i] > arreglo[j]) { // 3
      int auxiliar = arreglo[j]; // 2
      arreglo[j] = arreglo[i]; // 3
      arreglo[i] = auxiliar; // 2
    }
  }
}
```
- **Tiempo detallado**: 5n² - 5n
- **Tiempo asintótico**: O(n² - n)

## Ejercicio 15
>Eliminar el elemento en una posición de un arreglo

```c++
int* auxiliar = new int[size - 1]; // 3
int pos = 0; // 1
for (int i = 0; i < size; i++) { // n
  if (i != posicion) { // 1
      auxiliar[pos] = arreglo[i]; // 3
      pos++; // 2
  }
}
delete arreglo; // 1
arreglo = auxiliar; // 1
```
- **Tiempo detallado**: 6n + 6
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 16
>Buscar un número en un arreglo

```c++
int pos = -1; // 1
for (int i = 0; i < size; i++) { // n
  if (elemento == arreglo[i]) { // 2
      pos = i; // 1
  }
}
printf("La posicion del elemento es: %d", pos); // 1
```
- **Tiempo detallado**: 3n + 4
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 17
>Calcular el factorial de N

```c++
int factorial = 1; // 1
for (int i = 1; i <= numero; i++) { // n - 1
  factorial *= i; // 2
}
printf("El factorial es: %d\n", factorial); // 1
```
- **Tiempo detallado**: 2n
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 18
>Determinar si un número existe en un arreglo de enteros.

```c++
for (int i = 0; i < size; i++) { // n
  if (elemento == arreglo[i]) { // 3
      printf("El elemento si existe"); // 1
  }
}
printf("El elemento no existe"); // 1
```
- **Tiempo detallado**: 4n + 1
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 19
>Calcular cuántas veces se repite un número X en un arreglo de enteros.

```c++
int veces = 0; // 1
for (int i = 0; i < size; i++) { // n
  if (elemento == arreglo[i]) { // 2
      veces++; // 2
  }
}
printf("El elemento se repitio %d veces", veces); // 1
```
- **Tiempo detallado**: 4n + 2
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 20
>Sumar los dígitos de un número entero positivo.

```c++
int suma = 0; // 1
int contador = 0; // 1
while(n != 0) { // n
    contador++; // 2
    suma += n % 10; // 3
    n /= 10; // 2
}
printf("La suma de los digitos es: %d", suma); // 1
```
- **Tiempo detallado**: 7n + 3
- **Tiempo asintótico**: O(n) -> *Lineal*

## Ejercicio 21
>Determinar si un número es primo o no.

```c++
if(numero < 2) { // 1
  printf("El numero no es primo"); // 1
} else if(numero == 2) { // 1
  printf("El numero es primo"); // 1
} else if(numero % 2 == 0) { // 1
  printf("El numero no es primo"); // 1
}
for(int i = 3; (i * i) <= numero; i += 2) { // log(n)/2 - 3
  if(numero % i == 0 ) { // 2
    printf("El numero no es primo"); // 1
  }
}
printf("El numero es primo"); // 1
```
- **Tiempo detallado**: 3log(n)/2 - 2
- **Tiempo asintótico**: O(log(n)) -> *Logarítmico*

## Ejercicio 22
>Determinar la cantidad de primos que existen en un arreglo de enteros.

```c++
int primos = 0; // 1
for (int i = 0; i < size; i++) { // n
  if(numero < 2) { // 1
    continue;
  } else if(numero == 2) { // 1
    primos++; // 2
    continue;
  } else if(numero % 2 == 0) { // 2
    continue;
  }
  bool es_primo = true; // 2
  for(int i = 3; (i * i) <= numero; i += 2) { // log(n)/2 - 3
    if(numero % i == 0 ) { // 2
      es_primo = false; // 1
      break;
    }
  }
  if (es_primo) { // 1    
    primos++; // 2
  }
}
printf("Hay %d primos", primos); // 1
```
- **Tiempo detallado**: 3nlog(m) + 11n + 1
- **Tiempo asintótico**: O(nlog(m) + n)